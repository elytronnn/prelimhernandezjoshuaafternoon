package afternon.joshua.hernandez.prelim.prelimhernandezjoshuaafternoon;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class GuessingGame extends AppCompatActivity {


    Random random = new Random();
    final int randomInt = random.nextInt((20-1)+1)+1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guessing_game);
        setTitle("Guessing Game");

        Button btnTry = (Button)findViewById(R.id.btnTry);
        Button btnReveal = (Button)findViewById(R.id.btnReveal);
        final TextView txtInput = (TextView)findViewById(R.id.txtInputGG);
        final TextView txtInput2 = (TextView)findViewById(R.id.textView2);

        btnTry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (txtInput.length()>0){
                    String input = String.valueOf(txtInput.getText().toString());
                    int input_int = Integer.parseInt(input);


                    if (input_int > randomInt){
                        Toast.makeText(GuessingGame.this,"LOWER",Toast.LENGTH_SHORT).show();
                    }
                    if (input_int < randomInt){
                        Toast.makeText(GuessingGame.this,"HIGHER",Toast.LENGTH_SHORT).show();
                    }
                    if (input_int == randomInt){
                        Toast t = Toast.makeText(GuessingGame.this,"MATCHED",Toast.LENGTH_SHORT);
                        t.show();
                    }
                }
                }


        });

        btnReveal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String stringOfInput = String.valueOf(randomInt);
                txtInput2.setText(stringOfInput);
            }
        });
    }


}
