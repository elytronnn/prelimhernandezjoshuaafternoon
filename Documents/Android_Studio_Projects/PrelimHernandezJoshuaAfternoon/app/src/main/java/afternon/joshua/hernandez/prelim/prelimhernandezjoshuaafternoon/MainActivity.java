package afternon.joshua.hernandez.prelim.prelimhernandezjoshuaafternoon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnGuessingGame = (Button)findViewById(R.id.btnGG);
        Button btnReverser = (Button)findViewById(R.id.btnVS);
        Button btnTI = (Button)findViewById(R.id.btnTI);
        setTitle("Main Menu");

        btnGuessingGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this,GuessingGame.class);
                startActivity(myIntent);
            }
        });

        btnReverser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this,ReverseString.class);
                startActivity(myIntent);
            }
        });

        btnTI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this,Triangular.class);
                startActivity(myIntent);
            }
        });
    }
}
