package afternon.joshua.hernandez.prelim.prelimhernandezjoshuaafternoon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ReverseString extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reverse_string);
        final TextView txtInput = (TextView)findViewById(R.id.txtInputReverse);
        Button btnReverse = (Button)findViewById(R.id.btnReverse);
        setTitle("Reversal Of String");

        btnReverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toResult3 = new Intent(ReverseString.this,ReverseString2.class);
                toResult3.putExtra("lol",txtInput.getText().toString());
                startActivity(toResult3);
            }
        });


    }
}
