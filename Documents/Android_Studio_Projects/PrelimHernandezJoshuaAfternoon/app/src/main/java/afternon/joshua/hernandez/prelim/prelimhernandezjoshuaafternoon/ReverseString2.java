package afternon.joshua.hernandez.prelim.prelimhernandezjoshuaafternoon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class ReverseString2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reverse_string_2);
        Intent myIntent = getIntent();
        String myString = myIntent.getStringExtra("lol");
        setTitle("Reversal Of String");

        TextView txtOut = (TextView)findViewById(R.id.txtReverseOut);
        TextView txtCounter = (TextView)findViewById(R.id.txtCounter);

        String reverse = new StringBuilder(myString).reverse().toString();
        Log.d("TEST",reverse);

        //contain each letter
        HashMap<Character,Integer> hashCounter = new HashMap<>();
        for (int currentChar = 0; currentChar < reverse.length();currentChar++){
            char c = reverse.charAt(currentChar);
            if (hashCounter.containsKey(c)){
                int ctr = hashCounter.get(c);
                ctr++;
                hashCounter.put(c,ctr);
            }
            else{
                hashCounter.put(c,1);
            }
            //if contains then ++
            //else put
        }

        Log.d("TEST","myString length = " +reverse.length());
        Log.d("TEST","hashcounter size = " + hashCounter.size());

        txtOut.setText(reverse);
        String forOutPut="";
        for (Map.Entry<Character,Integer> entry: hashCounter.entrySet()){
            Character key = entry.getKey();
            Integer value = entry.getValue();

            if (value>1){
                String addString = key.toString() + " = " +value.toString();
                forOutPut = forOutPut + "\n" +addString;
            }
        }
        txtCounter.setText(forOutPut); //why
        Log.d("tester",forOutPut);
        Toast.makeText(ReverseString2.this,"string = " +forOutPut,Toast.LENGTH_SHORT).show();
    }
}
