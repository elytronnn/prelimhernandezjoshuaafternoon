package afternon.joshua.hernandez.prelim.prelimhernandezjoshuaafternoon;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Triangular extends AppCompatActivity {
    int input;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_triangular);
        setTitle("Triangular");

        final TextView txtInput = (TextView)findViewById(R.id.txtInput_Triangular);
        final TextView txtOutput = (TextView)findViewById(R.id.txtOutput_Triangular);
        Button btnCompute = (Button)findViewById(R.id.btnCompute_Triangular);
        Button btnDisplay = (Button)findViewById(R.id.btnDisplay_Triangular);

        btnCompute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtInput.length()>0){
                    input = Integer.valueOf(txtInput.getText().toString());
                    txtOutput.setText(triangleMaker(input));
                }
                else{
                    Toast.makeText(Triangular.this,"Must enter a number",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnDisplay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                txtOutput.setText(triangleMaker(10));
                Toast.makeText(Triangular.this,"Maximum level is 10",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String triangleMaker(int level) {
        String triangle="";

        if (level > 10){
            Toast.makeText(Triangular.this,"Maximum level is 10",Toast.LENGTH_SHORT).show();
            level = 10;
        }

        for (int i = 1; i <= level; i++){
            String currentLevel = i + " = " + (levelMaker(i)) + "\n";
            triangle = triangle + currentLevel;
        }
        return triangle;
    }

    private int levelMaker(int level){
        int levelReturn=0;
        for (int i = 1; i <= level; i++){
            levelReturn = levelReturn + i;
        }
        return levelReturn;
    }


}
